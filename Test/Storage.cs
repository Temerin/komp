﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Test
{
    public static class Storage
    {
        public static Dictionary<int, string> TokenDictionary => new Dictionary<int, string>
        {
            {0, "EOF"},
            {1, "ident"},
            {2, "arrc"},
            {3, "funcident"},
            {4, "intcon"},
            {5, "realcon"},
            {6, "strcon"},
            {7, "plus"},
            {8, "minus"},
            {9, "times"},
            {10, "div"},
            {11, "startbool"},
            {12, "startfunc"},
            {13, "endfunc"},
            {14, "endbool"},
            {15, "begin"},
            {16, "end"},
            {17, "semicol"},
            {18, "startpoint"},
            {19, "endpoint"},
            {20, "blocksize"},
            {21, "and"},
            {22, "or"},
            {23, "not"},
            {24, "assumeSign"},
            {25, "readName"},
            {26, "writeName"},
            {27, "varBlockName"},
            {28, "funcBlockName"},
            {29, "returnName"},
            {30, "funcListName"},
            {31, "ifName"},
            {32, "mainNameStart"},
            {33, "mainNameFin"},
            {34, "elseName"},
            {35, "whileName"},
            {36, "array"},
            {37, "int"},
            {38, "float"},
            {39, "string"}
        };
        
        public static Dictionary<string, string> LsLangToCilTypes => new Dictionary<string, string>
        {
            {"INT", "int32"},
            {"STR", "string"},
            {"VOYAGE", "float64"},
            {"void", "void"}
        };

        public static Dictionary<string, string> LsLangToDotNetTypes => new Dictionary<string, string>
        {
            {"INT", "Int32"},
            {"STR", "String"},
            {"VOYAGE", "Double"},
            {"void", "void"}
        };

        public static Dictionary<string, string> LsLangToCilDownload => new Dictionary<string, string>
        {
            {"INT", "ldc.i4 "},
            {"BlockPoint", "ldc.i4 "},
            {"STR", "ldstr "},
            {"VOYAGE", "ldc.r8 "},
            {"VarName", "ldsfld"}
        };

        public static Dictionary<string, string> LsLangToCilOperations = new Dictionary<string, string>
        {
            {"+", "add" },
            {"-", "sub" },
            {"*", "mul" },
            {"<", "clt" },
            {">", "cgt" },
            {"==", "ceq" },
            {"AND", "and" },
            {"OR", "or" },
            {"NOT", "not" }
        };

        public static Tree Tree { get; set; }
        public static List<Token> Tokens { get; set; } = new List<Token>();

        public static string GetProjectNamespace
        {
            get
            {
                var innerIdent = Tokens.Any() ? Tokens.FirstOrDefault(x => TokenDictionary[x.kind] == "ident") : null;
                return innerIdent != null ? innerIdent.val : "";
            }
        }
        public static Dictionary<string, Variable> IdentifiersDict { get; set; } = new Dictionary<string, Variable>();
        public static Dictionary<string, Action> ActionsDict { get; set; } = new Dictionary<string, Action>();

        public static string Stringify(this List<Token> list)
        {
            return list.Aggregate("", (current, token) => current + token.Stringify());
        }

        public static string Stringify(this Token token)
        {
            return $"Имя: {TokenDictionary[token.kind]}; Строка: {token.line}; Значение: {token.val}\r\n";
        }

        public static string Stringify(this Dictionary<string, Variable> dict)
        {
            return dict.Aggregate("", (current, tuple) => current + tuple.Stringify());
        }

        public static string Stringify(this KeyValuePair<string, Variable> tuple)
        {
            if (tuple.Value.ArrayType == null)
            {
                string varValue;
                switch (tuple.Value.Type)
                {
                    case "INT":
                        varValue = tuple.Value.Value.IValue.ToString();
                        break;
                    case "VOYAGE":
                        varValue = tuple.Value.Value.FValue.ToString(CultureInfo.InvariantCulture);
                        break;
                    case "STR":
                        varValue = tuple.Value.Value.SValue;
                        break;
                    default:
                        varValue = "";
                        break;
                }
                return $"Тип: {tuple.Value.Type}; Имя: {tuple.Value.Name}; Значение: {varValue}\r\n";
            }
            else
            {
                return $"Тип: {tuple.Value.Type}; Имя: {tuple.Value.Name};\r\n";
            }
        }
    }
}
