﻿using System.Collections.Generic;
using System.Xml;

namespace Test
{
    public static class XmlGateway
    {
        private static KeyValuePair<string, TransformationTree> _cache = new KeyValuePair<string, TransformationTree>();

        private static XmlDocument _xmlDoc = new XmlDocument();
        public static TransformationTree GetByKey(string key)
        {
            TransformationTree result = null;
            if (key == _cache.Key)
                return _cache.Value;
            _xmlDoc.Load("CilDatabase.xml");
            var el0 = _xmlDoc.SelectSingleNode("/dictionary/record[@key='" + key + "']");
            if (el0 != null)
            {
                result = new TransformationTree
                {
                    Begin = el0.SelectSingleNode("begin")?.InnerText,
                    End = el0.SelectSingleNode("end")?.InnerText
                };
            }
            _cache = new KeyValuePair<string, TransformationTree>(key, result);
            return result;
        }
    }
}
