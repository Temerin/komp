﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Test
{
    public class Tree
    {
        public Node Root;
        Node _current;

        public Tree(string name, Token token){
            this.Root = new Node(name, "", null, token.line);
            _current = Root;
            Storage.Tokens.Add(token);
        }

        public void Down(string name, string value, Token token)
        {
            _current.Children.Add(new Node(name, value, _current, token.line));
            _current = _current.Children.Last();
            Storage.Tokens.Add(token);
        }

        public void Up(int n = 1)
        {
            for (var i = 0; i < n; i++)
                _current = _current.Parent;
        }

        public string SetIndentGlobal(int spaces)
        {
            return Root.SetIndent(spaces);
        }
    }

    public class Node
    {
        public List<Node> Children;
        public Node Parent;
        public string Name;
        public string Value;
        public int Line;

        public Node(string name, string value, Node parent = null, int line = -1)
        {
            this.Parent = parent;
            this.Name = name;
            this.Value = value;
            this.Line = line;
            this.Children = new List<Node>();
        }

        public string SetIndent(int spaces)
        {
            var result = new String('\t', spaces);
            result += $"Имя: {Name}";
            if (Value != "")
                result += $" Значение: {Value}";
            result += $" Строка: {Line}\r\n";
            return Children.Aggregate(result, (current, child) => current + child.SetIndent(spaces + 1));
        }

        public List<Node> GetByName(string name)
        {
            var result = new List<Node>();
            if (this.Name != name)
                this.Children.ForEach(x => result.AddRange(x.GetByName(name)));
            else
                result.Add(this);
            return result;
        }

        public List<Node> GetByValue(string name)
        {
            var result = new List<Node>();
            if (this.Value != name)
                this.Children.ForEach(x => result.AddRange(x.GetByValue(name)));
            else
                result.Add(this);
            return result;
        }
    }
}
