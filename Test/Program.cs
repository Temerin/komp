﻿using System;
using System.Windows.Forms;

namespace Test
{
    static class Program
    {
        public static string[] AppArguments { get; private set; }

        [STAThread]
        static void Main(string[] args)
        {
            AppArguments = new string[args.Length];
            for (int i = 0; i < AppArguments.Length; i++)
            {
                AppArguments[i] = args[i];
            }
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Test());
        }
    }
}
