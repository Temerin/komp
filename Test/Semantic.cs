﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Test
{
	public class Semantic
	{
		public string Analize(Node code)
		{
			var result = this.AnalizeVarBlock(code, Storage.IdentifiersDict, "VarDeclareBlock");
			if (code.GetByName("FuncDeclareBlock").Count>0)
				result += this.AnalizeActivity(code);
			result += this.AnalizeMainBlock(code, Storage.IdentifiersDict, "MainBlock");
			return result;
		}

	    private string AnalizeVarBlock(Node code, Dictionary<string, Variable> dictVars, string blockName)
		{
			var result ="";
			var variables = code.GetByName(blockName);
            foreach (var node in variables.First().Children)
	        {
	            var varType = node.GetByName("VarType");
	            var varName = node.GetByName("VarName");
	            var tempVar = new Variable(varName.First().Value, varType.First().Value);
	            if (tempVar.Type == "ARRAY")
	            {
	                var arrayType = node.GetByName("blockType");
	                tempVar.ArrayType = arrayType.First().Value;
	                tempVar.Value.IValue = Convert.ToInt32(node.GetByName("blocksize").FirstOrDefault()?.Value ?? "0");

	            }	                
                if (!dictVars.ContainsKey(varName.First().Value))
                    dictVars.Add(varName.First().Value, tempVar);
	            else
	                result += $"Повторное объявление переменной на строке {node.Line}\r\n";
	        }
	        return result;
		}

	    private string AnalizeActivity(Node code)
		{
			var result = "";
			var functions = code.GetByName("FuncDeclareBlock");
			foreach (var func in functions.First().Children)
			{
				var funcName = func.GetByName("FuncName");
                var tempFunc = new Action(funcName.First().Value, func.GetByName("FuncRetType").First().Value);
                result += this.AnalizeVarBlock(func.GetByName("FuncVarBlock").First(), tempFunc.Vars, "FuncVarBlock");
				if (!Storage.ActionsDict.ContainsKey(funcName.First().Value))
				{
					Storage.ActionsDict.Add(funcName.First().Value, tempFunc);
					result += this.AnalizeMainBlock(func.GetByName("FuncStatements").First(), tempFunc.Vars, "FuncStatements");
					result += CheckYield(func, Storage.ActionsDict, tempFunc.Vars);
				}
                else
					result += $"Повторное объявление функции на строке {func.Line}\r\n";
			}
			return result;
		}

	    private string CheckYield(Node func, Dictionary<string, Action> funcList, Dictionary<string, Variable> varList)
		{
			var result = "";
			var funcName = func.GetByName("FuncName");
			if (funcList.ContainsKey(funcName.First().Value))
			{
				var testFunc = funcList[funcName.First().Value];
				var returnBlock = func.GetByName("ReturnBlock");
				if (testFunc.Vars.Count == 0 )
				{
					var varName = returnBlock.First().GetByName("VarName");
				    if (Storage.IdentifiersDict.ContainsKey(varName.First().Value))
				    {
				        if (Storage.IdentifiersDict[varName.First().Value].Type != testFunc.ReturnType)
				            result += $"Несоответствие типов для возвращаемого значения на строке {funcName.First().Line}\r\n";
				    }
				    else
				        result += $"Необъявленная переменная на строке {varName.First().Line}\r\n";
				}
				else
				{
					var varName = returnBlock.First().GetByName("VarName");	
					if (varList.ContainsKey(varName.First().Value))
					{
						if (varList[varName.First().Value].Type != testFunc.ReturnType)
							result += $"Несоответствие типов для возвращаемого значения на строке {funcName.First().Line}\r\n";
					}
					else
					{
						if (Storage.IdentifiersDict.ContainsKey(varName.First().Value))
						{
							if (Storage.IdentifiersDict[varName.First().Value].Type != testFunc.ReturnType)
								result += $"Несоответствие типов для возвращаемого значения на строке {funcName.First().Line}\r\n";
						}
						else
							result += $"Необъявленная переменная на строке{varName.First().Line}\r\n";
					}	
				}
			}
			else
                result += $"Необъявленная функция на строке {funcName.First().Line}\r\n";
            return result;
		}

	    private string CheckVar(Node varName, Dictionary<string, Variable> varsList)
		{
			return !varsList.ContainsKey(varName.Value) ? $"Необъявленная переменная на строке {varName.Line}\r\n" : "";
		}

	    private string CheckFunc(Node funcName, Dictionary<string, Action> funcList)
		{
			return !funcList.ContainsKey(funcName.Value) ? $"Необъявленная функция на строке {funcName.Line}\r\n" : "";
		}

	    private string AnalizeMainBlock(Node code, Dictionary<string, Variable> varDict, string statementsBlock)
		{
            var aAnalyze = new AssumeAnalyzer(Storage.ActionsDict, varDict);
	        var statements = code.GetByName(statementsBlock);
			var usedVars = statements.First().GetByName("VarName");
	        var result = usedVars.Aggregate("", (current, var) => current + this.CheckVar(var, varDict));
	        var usedFuncs = statements.First().GetByName("FuncCall");
	        result = usedFuncs.Aggregate(result, (current, func) => current + this.CheckFunc(func, Storage.ActionsDict));
	        var index = -1;
			foreach (var statement in statements.First().Children)
			{
				switch (statement.Name)
				{
					case "Assume":
						result += aAnalyze.CheckAssume(statement);
						break;
					case "If":
						result += this.AnalizeMainBlock(statement, varDict, "If");
						this.CheckBoolValueForReach(statement, varDict);
						break;
					case "Else":
						result += this.AnalizeMainBlock(statement, varDict, "Else");
						break;
					case "While":
						result += this.AnalizeMainBlock(statement.Parent, varDict, "While");
						break;
					default:
						break;
				}
			}
			if (index != -1)
				code.Parent.Children.Remove(code.Parent.Children[index]);
			
			return result;
		}

	    private void CheckBoolValueForReach(Node code, Dictionary<string, Variable> varDict)
		{	
			var boolExp = code.GetByName("BoolExp");
			var boolExpString = "";
			var temp = new Variable();
			foreach (var child in boolExp.First().Children)
			{
			    if (child.Name == "VarName")
			    {
			        try
			        {
			            temp = varDict[child.Value];
			            var index = boolExp.First().Children.IndexOf(child);
			            switch (temp.Type)
			            {
			                case "INT":
			                    if ((index - 1 > 0) &&
			                        (boolExp.First().Children[index - 1].Value == ">" || boolExp.First().Children[index - 1].Value == "<"))
			                        boolExpString += $" {temp.Value.IValue} ";
			                    else if ((index + 1 < boolExp.First().Children.Count) &&
			                             (boolExp.First().Children[index + 1].Value == ">" ||
			                              boolExp.First().Children[index + 1].Value == "<"))
			                        boolExpString += $" {temp.Value.IValue} ";
			                    else
			                    {
			                        if (temp.Value.IValue > 0)
			                            boolExpString += " TRUE ";
			                        else
			                            boolExpString += " FALSE ";
			                    }

			                    break;
			                case "STR":
			                    boolExpString += " TRUE ";
			                    break;
			                case "VOYAGE":

			                    if ((index - 1 > 0) &&
			                        (boolExp.First().Children[index - 1].Value == ">" || boolExp.First().Children[index - 1].Value == "<"))
			                        boolExpString += $" {temp.Value.FValue} ";
			                    else if ((index + 1 < boolExp.First().Children.Count) &&
			                             (boolExp.First().Children[index + 1].Value == ">" ||
			                              boolExp.First().Children[index + 1].Value == "<"))
			                        boolExpString += $" {temp.Value.FValue} ";
			                    else
			                    {
			                        if (temp.Value.FValue > 0)
			                            boolExpString += " TRUE ";
			                        else
			                            boolExpString += " FALSE ";
			                    }
			                    break;
			                default:
			                    break;
			            }
			        }
			        catch (Exception)
			        {
			        }
			    }
			    else if (child.Name != "BoolSign")
			    {
			        var index = boolExp.First().Children.IndexOf(child);

			        switch (child.Name)
			        {
			            case "INT":

			                if ((index - 1 > 0) &&
			                    (boolExp.First().Children[index - 1].Value == ">" || boolExp.First().Children[index - 1].Value == "<"))
			                    boolExpString += $" {child.Value} ";
			                else if ((index + 1 < boolExp.First().Children.Count) &&
			                         (boolExp.First().Children[index + 1].Value == ">" || boolExp.First().Children[index + 1].Value == "<"))
			                    boolExpString += $" {child.Value} ";
			                else
			                {
			                    if (Convert.ToInt32(child.Value) > 0)
			                        boolExpString += " TRUE ";
			                    else
			                        boolExpString += " FALSE ";
			                }

			                break;
			            case "STR":
			                boolExpString += " TRUE ";
			                break;
			            case "VOYAGE":
			                if ((index - 1 > 0) &&
			                    (boolExp.First().Children[index - 1].Value == ">" || boolExp.First().Children[index - 1].Value == "<"))
			                    boolExpString += $" {child.Value} ";
			                else if ((index + 1 < boolExp.First().Children.Count) &&
			                         (boolExp.First().Children[index + 1].Value == ">" || boolExp.First().Children[index + 1].Value == "<"))
			                    boolExpString += $" {child.Value} ";
			                else
			                {
			                    if (Convert.ToDouble(child.Value) > 0)
			                        boolExpString += " TRUE ";
			                    else
			                        boolExpString += " FALSE ";
			                }
			                break;
			            default:
			                break;
			        }
			    }
			    else
			        boolExpString += child.Value;
			}
		}
	}
}
