﻿using System.Collections.Generic;

namespace Test
{
    public class TransformationTree
    {
        public string Begin { get; set; }

        public string End { get; set; }

        public List<TransformationTree> Content { get; set; }

        public string Render()
        {
            var result = Begin;
            foreach (var node in Content)
            {
                result += node.Render();
            }
            result += End;
            return result;
        } 
    }
}
