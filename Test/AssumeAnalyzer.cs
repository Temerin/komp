﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Test
{
    public sealed class AssumeAnalyzer
    {
        private Dictionary<string, Action> _decActions;
        Dictionary<string, Variable> _decVariables;
        private Variable _tempVar;
        Action _tempFunc;
        string _result;
        private string _leftType;
        private string _rightType;
        private string _assumeExp;

        public AssumeAnalyzer(Dictionary<string, Action> actions, Dictionary<string, Variable> decVariables)
        {
            _decActions = actions;
            _result = "";
            _leftType = _rightType = "";
            _assumeExp = "";
            _tempVar = new Variable();
            _decVariables = decVariables;
        }

        public string CheckAssume(Node assign)
        {
            if (_decVariables.ContainsKey(assign.Children.First().Value))
                _tempVar = _decVariables[assign.Children.First().Value];
            else
                _result += $"Необъявленная переменная на строке {assign.Children.First().Line}\r\n";
            _leftType = _tempVar.Type;
            if (_leftType == "ARRAY")
            {
                if (assign.Children.First().Children.First().Value != "<")
                {
                    if (_decVariables.ContainsKey(assign.Children.First().Value))
                        _leftType = _decVariables[assign.Children.First().Value].ArrayType;
                    else
                        _result += $"Необъявленная переменная на строке {assign.Children.First().Line}\r\n";
                }
                else
                    _leftType = _tempVar.Type;
            }
            if (assign.GetByName("Calc").Count > 0)
                PrimaryBranch(assign);
            else
                SecondBranch(assign);
            return _result;
        }

        private void PrimaryBranch(Node assign)
        {
            var rightParts = new List<string>();
            foreach (var rightPart in assign.Children.Where(rightPart => rightPart.Name != "Calc"))
                AnalyzeRightPart(assign, rightPart, rightParts);
            if (!CompareComplexTypes(rightParts))
            {
                var finalComplexType = "";
                if (rightParts.Contains("STR"))
                {
                    finalComplexType = "STR";
                    _result += $"Математическая операция над строками на строке {assign.Children.First().Line}\r\n";
                }
                else if (rightParts.Contains("INT") || rightParts.Contains("VOYAGE"))
                    finalComplexType = "VOYAGE";
                if (!CompareTypes(_leftType, finalComplexType))
                {
                    for (var i = 1; i < assign.Children.Count; i++)
                        CheckChildren(assign, i);
                    var table = new System.Data.DataTable();
                    table.Columns.Add("expression", string.Empty.GetType(), _assumeExp);
                    var row = table.NewRow();
                    table.Rows.Add(row);
                    var left = _decVariables[assign.Children.First().Value];

                    switch (left.Type)
                    {
                        case "STR":
                            break;
                        case "INT":
                            int iresult;
                            if (!Int32.TryParse((string) row["expression"], out iresult))
                                _result += $"Несоответствие типов на строке {assign.Children.Last().Line}\r\n";
                            else
                                _decVariables[left.Name].Value.IValue = iresult;
                            break;
                        case "VOYAGE":
                            double dresult;
                            if (!Double.TryParse((string) row["expression"], out dresult))
                                _result += $"Несоответствие типов на строке {assign.Children.Last().Line}\r\n";
                            _decVariables[left.Name].Value.FValue = dresult;
                            break;
                        default:
                            break;
                    }
                }
                else
                    _result += $"Несоответствие типов на строке {assign.Children.Last().Line}\r\n";
            }
            else
                _result += $"Несоответствие типов на строке {assign.Children.First().Line}\r\n";
        }

        private void CheckChildren(Node assign, int childIndex)
        {
            switch (assign.Children[childIndex].Name)
            {
                case "VarName":
                    if (_tempVar.Type != "ARRAY")
                    {
                        if (_decVariables.ContainsKey(assign.Children[childIndex].Value))
                        {
                            var tempRight = _decVariables[assign.Children[childIndex].Value];
                            if (tempRight.Value.FValue < Double.MaxValue)
                                _assumeExp += tempRight.Value.FValue.ToString();
                            else if (tempRight.Value.IValue != Int32.MaxValue)
                                _assumeExp += tempRight.Value.IValue.ToString();
                            else if (tempRight.Value.SValue != null)
                                _assumeExp += tempRight.Value.SValue;
                        }
                    }
                    break;
                case "FuncCall":
                    break;
                default:
                    switch (assign.Children[childIndex].Name)
                    {
                        case "STR":
                        case "Calc":
                        case "INT":
                            _assumeExp += assign.Children[childIndex].Value;
                            break;
                        case "VOYAGE":
                            if (_decVariables[assign.Children.First().Value].Type == "INT")
                            {
                                var provider = new NumberFormatInfo {NumberDecimalSeparator = "."};
                                _assumeExp +=
                                    Convert.ToInt32(Math.Round(Convert.ToDouble(assign.Children[childIndex].Value, provider)))
                                        .ToString();
                            }
                            else
                                _assumeExp += assign.Children[childIndex].Value;
                            break;
                        default:
                            break;
                    }
                    break;
            }
        }

        private void AnalyzeRightPart(Node assign, Node rightPart, List<string> rightParts)
        {
            switch (rightPart.Name)
            {
                case "VarName":
                    _tempVar = _decVariables[rightPart.Value];
                    if (_tempVar.Type == "ARRAY")
                        rightParts.Add(rightPart.Children.First().Value != "<" ? _tempVar.ArrayType : _tempVar.Type);
                    else
                        rightParts.Add(_tempVar.Type);
                    break;
                case "FuncCall":
                    if (_decActions.ContainsKey(rightPart.Value))
                    {
                        _tempFunc = _decActions[rightPart.Value];
                        rightParts.Add(_tempFunc.ReturnType);
                        var funcGet = new List<string>();
                        foreach (var var in assign.Children.Last().Children)
                        {
                            _rightType = var.Name;
                            if (_rightType == "VarName")
                            {
                                if (_decVariables.ContainsKey(var.Value))
                                    funcGet.Add(_decVariables[var.Value].Type);
                                else
                                    _result += $"Необъявленная переменная на строке {assign.Children.First().Line}\r\n";
                            }
                        }
                        _result += CheckFuncInputs(assign.Children.Last(), _decActions, funcGet);
                        rightParts.Add(_tempFunc.ReturnType);
                    }
                    else
                        _result += $"Необъявленная переменная на строке {assign.Children.First().Line}\r\n";
                    if (CompareTypes(_leftType, _rightType))
                        _result += $"Несоответствие типов на строке {assign.Children.Last().Line}\r\n";
                    break;
                default:
                    rightParts.Add(rightPart.Name);
                    break;
            }
        }

        private void SecondBranch(Node assign)
        {
            _rightType = assign.Children.Last().Name;
            switch (_rightType)
            {
                case "VarName":
                    _tempVar = _decVariables[assign.Children.Last().Value];
                    _rightType = _tempVar.Type;
                    if (_rightType == "ARRAY")
                    {
                        if (assign.Children.Last().Children.First().Value != "<")
                        {
                            if (_decVariables.ContainsKey(assign.Children.Last().Value))
                                _rightType = _decVariables[assign.Children.Last().Value].ArrayType;
                            else
                                _result += $"Необъявленная переменная на строке{assign.Children.First().Line}\r\n";
                            if (CompareTypes(_leftType, _rightType))
                                _result += $"Несоответствие типов на строке {assign.Children.Last().Line}\r\n";
                        }
                        else if (CompareTypes(_leftType, _rightType))
                            _result += $"Несоответствие типов на строке {assign.Children.Last().Line}\r\n";
                    }
                    else if (CompareTypes(_leftType, _rightType))
                        _result += $"Несоответствие типов на строке {assign.Children.Last().Line}\r\n";
                    var right = _decVariables[assign.Children.Last().Value];
                    var left = _decVariables[assign.Children.First().Value];

                    switch (_rightType)
                    {
                        case "STR":
                            if (right.Value.SValue != null)
                                _decVariables[left.Name].Value.SValue = right.Value.SValue;
                            else
                                _result += $"Операция с null на строке {assign.Children.First().Line}\r\n";
                            break;
                        case "INT":
                            if (right.Value.IValue != Int32.MaxValue)
                                _decVariables[left.Name].Value.IValue = right.Value.IValue;
                            else
                                _result += $"Операция с null на строке {assign.Children.First().Line}\r\n";
                            break;
                        case "VOYAGE":
                            if (right.Value.FValue < Double.MaxValue)
                                _decVariables[left.Name].Value.FValue = right.Value.FValue;
                            else
                                _result += $"Операция с null на строке {assign.Children.First().Line}\r\n";
                            break;
                        default:
                            break;
                    }
                    break;
                case "FuncCall":
                    if (_decActions.ContainsKey(assign.Children.Last().Value))
                    {
                        _tempFunc = _decActions[assign.Children.Last().Value];
                        var funcGet = new List<string>();
                        foreach (var node in assign.Children.Last().Children)
                        {
                            _rightType = node.Name;
                            if (_rightType == "VarName")
                            {
                                if (_decVariables.ContainsKey(node.Value))
                                    funcGet.Add(_decVariables[node.Value].Type);
                                else
                                    _result += $"Необъявленная переменная на строке {assign.Children.First().Line}\r\n";
                            }
                        }
                        _result += CheckFuncInputs(assign.Children.Last(), _decActions, funcGet);
                        _rightType = _tempFunc.ReturnType;
                    }
                    else
                        _result += $"Необъявленная переменная на строке {assign.Children.First().Line}\r\n";
                    if (CompareTypes(_leftType, _rightType))
                        _result += $"Несоответствие типов на строке {assign.Children.Last().Line}\r\n";
                    break;
                default:
                    if (!CompareTypes(_leftType, _rightType))
                    {
                        switch (_rightType)
                        {
                            case "STR":
                                _decVariables[_tempVar.Name].Value.SValue = assign.Children.Last().Value;
                                break;
                            case "INT":
                                _decVariables[_tempVar.Name].Value.IValue = Convert.ToInt32(assign.Children.Last().Value);
                                break;
                            case "VOYAGE":
                                _decVariables[_tempVar.Name].Value.FValue = Convert.ToDouble(assign.Children.Last().Value);
                                break;
                            default:
                                break;
                        }
                    }
                    else
                        _result += $"Несоответствие типов на строке {assign.Children.Last().Line}\r\n";
                    break;
            }
        }

        private string CheckFuncInputs(Node funcName, Dictionary<string, Action> funcList, List<string> funcGets)
        {
            var result = "";
            var funcNeed = new List<string>();
            if (funcList.ContainsKey(funcName.Value))
                funcNeed = new List<string>(funcList[funcName.Value].GetInputTypes());
            else
                result += $"Необъявленная функция на строке {funcName.Line}\r\n";
            if (funcNeed.Count != funcGets.Count)
                result += $"Неверное количество аргументов на строке {funcName.Line}\r\n";
            else
                result = funcNeed.Select((t, i) => CompareTypes(t, funcGets[i]))
                    .Where(checkType => checkType)
                    .Aggregate(result, (current, checkType) => ($"{current} Несоответствие типов в аргументах функции на строке {funcName.Line}\r\n"));
            return result;
        }

        private bool CompareComplexTypes(List<string> types)
        {
            return types.Any(type => types.Any(t => CompareTypes(type, t)));
        }

        private bool CompareTypes(string leftType, string rightType)
        {
            var result = false;
            switch (leftType)
            {
                case "INT":
                    if (rightType == "STR" || rightType == "ARRAY")
                        result = true;
                    break;
                case "VOYAGE":
                    if (rightType == "STR" || rightType == "ARRAY")
                        result = true;
                    break;
                case "STR":
                    if (rightType == "INT" || rightType == "VOYAGE" || rightType == "ARRAY")
                        result = true;
                    break;
                case "ARRAY":
                    if (rightType == "INT" || rightType == "VOYAGE" || rightType == "STR")
                        result = true;
                    break;
                default:
                    break;
            }
            return result;
        }
    }
}
