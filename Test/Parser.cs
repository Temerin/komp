
using System;

namespace Test
{



public class Parser {
	public const int _EOF = 0;
	public const int _ident = 1;
	public const int _arrc = 2;
	public const int _funcident = 3;
	public const int _intcon = 4;
	public const int _realcon = 5;
	public const int _strcon = 6;
	public const int _plus = 7;
	public const int _minus = 8;
	public const int _times = 9;
	public const int _div = 10;
	public const int _startbool = 11;
	public const int _startfunc = 12;
	public const int _endfunc = 13;
	public const int _endbool = 14;
	public const int _begin = 15;
	public const int _end = 16;
	public const int _semicol = 17;
	public const int _startpoint = 18;
	public const int _endpoint = 19;
	public const int _blocksize = 20;
	public const int _and = 21;
	public const int _or = 22;
	public const int _not = 23;
	public const int _assumeSign = 24;
	public const int _readName = 25;
	public const int _writeName = 26;
	public const int _varBlockName = 27;
	public const int _funcBlockName = 28;
	public const int _returnName = 29;
	public const int _funcListName = 30;
	public const int _ifName = 31;
	public const int _mainNameStart = 32;
	public const int _mainNameFin = 33;
	public const int _elseName = 34;
	public const int _whileName = 35;
	public const int _array = 36;
	public const int _int = 37;
	public const int _float = 38;
	public const int _string = 39;
	public const int maxT = 43;

	const bool _T = true;
	const bool _x = false;
	const int minErrDist = 2;
	
	public Scanner scanner;
	public Errors  errors;

	public Token t;    // last recognized token
	public Token la;   // lookahead token
	int errDist = minErrDist;



	public Parser(Scanner scanner) {
		this.scanner = scanner;
		errors = new Errors();
	}

	void SynErr (int n) {
		if (errDist >= minErrDist) errors.SynErr(la.line, la.col, n);
		errDist = 0;
	}

	public void SemErr (string msg) {
		if (errDist >= minErrDist) errors.SemErr(t.line, t.col, msg);
		errDist = 0;
	}
	
	void Get () {
		for (;;) {
			t = la;
			la = scanner.Scan();
			if (la.kind <= maxT) { ++errDist; break; }

			la = t;
		}
	}
	
	void Expect (int n) {
		if (la.kind==n) Get(); else { SynErr(n); }
	}
	
	bool StartOf (int s) {
		return set[s, la.kind];
	}
	
	void ExpectWeak (int n, int follow) {
		if (la.kind == n) Get();
		else {
			SynErr(n);
			while (!StartOf(follow)) Get();
		}
	}


	bool WeakSeparator(int n, int syFol, int repFol) {
		int kind = la.kind;
		if (kind == n) {Get(); return true;}
		else if (StartOf(repFol)) {return false;}
		else {
			SynErr(n);
			while (!(set[syFol, kind] || set[repFol, kind] || set[0, kind])) {
				Get();
				kind = la.kind;
			}
			return StartOf(syFol);
		}
	}

	
	void Test() {
		Storage.Tree = new Tree("Start", t); 
		Expect(40);
		Expect(1);
		if (la.kind == 27) {
			VarBlock();
		}
		if (la.kind == 28) {
			FuncBlock();
		}
		MainBlock();
		Expect(41);
	}

	void VarBlock() {
		Storage.Tree.Down("VarDeclareBlock", "VARIABLES", t); 
		Expect(27);
		Expect(15);
		VarList();
		while (StartOf(1)) {
			VarList();
		}
		Expect(16);
		Expect(17);
		Storage.Tree.Up(); 
	}

	void FuncBlock() {
		Storage.Tree.Down("FuncDeclareBlock", "ACTIVITY", t); 
		Expect(28);
		Expect(15);
		FuncList();
		while (la.kind == 30) {
			FuncList();
		}
		Expect(16);
		Expect(17);
		Storage.Tree.Up(); 
	}

	void MainBlock() {
		Storage.Tree.Down("MainBlock", "LAUNCH", t); 
		Expect(32);
		Expect(15);
		while (StartOf(2)) {
			Action();
		}
		Expect(16);
		Expect(17);
		Expect(33);
		Expect(17);
		Storage.Tree.Up(); 
	}

	void Action() {
		if (la.kind == 25) {
			Read();
		} else if (la.kind == 26) {
			Write();
		} else if (la.kind == 1 || la.kind == 2) {
			Assume();
		} else if (la.kind == 31) {
			If();
		} else if (la.kind == 35) {
			While();
		} else SynErr(44);
	}

	void VarName() {
		if (la.kind == 1) {
			Get();
			Storage.Tree.Down("VarName", t.val, t); 
			Storage.Tree.Up(); 
		} else if (la.kind == 2) {
			ARRAY();
		} else SynErr(45);
	}

	void ARRAY() {
		Expect(2);
		Storage.Tree.Down("VarName", t.val, t); 
		Expect(18);
		if (la.kind == 1 || la.kind == 2 || la.kind == 4) {
			if (la.kind == 1 || la.kind == 2) {
				VarName();
			} else {
				Get();
			}
		}
		Storage.Tree.Down("BlockPoint", t.val, t); 
		Storage.Tree.Up(); 
		Expect(19);
		Storage.Tree.Up(); 
	}

	void VarProduct() {
		if (la.kind == 1 || la.kind == 2) {
			VarName();
		} else if (la.kind == 6) {
			Get();
			Storage.Tree.Down("STR", t.val, t); 
			Storage.Tree.Up(); 
		} else if (la.kind == 4) {
			Get();
			Storage.Tree.Down("INT", t.val, t); 
			Storage.Tree.Up(); 
		} else if (la.kind == 5) {
			Get();
			Storage.Tree.Down("VOYAGE", t.val, t); 
			Storage.Tree.Up(); 
		} else SynErr(46);
	}

	void blockDeclare() {
		blockType();
		Expect(2);
		Storage.Tree.Down("VarName", t.val, t); 
		Expect(20);
		Expect(4);
		Storage.Tree.Down("blocksize", t.val, t); 
		Storage.Tree.Up(); 
		Expect(20);
		Storage.Tree.Up(); 
	}

	void blockType() {
		if (la.kind == 37) {
			Get();
		} else if (la.kind == 38) {
			Get();
		} else if (la.kind == 39) {
			Get();
		} else SynErr(47);
		Storage.Tree.Down("blockType", t.val, t); 
		Storage.Tree.Up(); 
	}

	void VarList() {
		Storage.Tree.Down("Variable", "", t); 
		Type();
		VarDeclare();
		Expect(17);
		Storage.Tree.Up(); 
	}

	void Type() {
		if (la.kind == 37) {
			Get();
		} else if (la.kind == 38) {
			Get();
		} else if (la.kind == 39) {
			Get();
		} else if (la.kind == 36) {
			Get();
		} else SynErr(48);
		Storage.Tree.Down("VarType", t.val, t); 
		Storage.Tree.Up(); 
	}

	void VarDeclare() {
		if (la.kind == 1) {
			Get();
			Storage.Tree.Down("VarName", t.val, t); 
			Storage.Tree.Up(); 
		} else if (la.kind == 37 || la.kind == 38 || la.kind == 39) {
			blockDeclare();
		} else SynErr(49);
	}

	void FuncRetType() {
		if (la.kind == 37) {
			Get();
		} else if (la.kind == 38) {
			Get();
		} else if (la.kind == 39) {
			Get();
		} else if (la.kind == 36) {
			Get();
		} else SynErr(50);
		Storage.Tree.Down("FuncRetType", t.val, t); 
		Storage.Tree.Up(); 
	}

	void FuncList() {
		Expect(30);
		FuncBody();
		Expect(17);
	}

	void FuncBody() {
		Storage.Tree.Down("Function", "", t); 
		FuncRetType();
		Expect(3);
		Storage.Tree.Down("FuncName", t.val, t); 
		Storage.Tree.Up(); 
		Expect(12);
		FuncVarBlock();
		Expect(13);
		Expect(15);
		Storage.Tree.Down("FuncStatements", "", t); 
		while (StartOf(2)) {
			Action();
		}
		Storage.Tree.Up(); 
		Return();
		Expect(16);
		Storage.Tree.Up(); 
	}

	void FuncVarBlock() {
		Storage.Tree.Down("FuncVarBlock", "", t); 
		while (StartOf(1)) {
			VarList();
		}
		Storage.Tree.Up(); 
	}

	void Return() {
		Storage.Tree.Down("ReturnBlock", "GIVE", t); 
		Expect(29);
		VarName();
		Expect(17);
		Storage.Tree.Up(); 
	}

	void Read() {
		Storage.Tree.Down("Read", "OBTAIN", t); 
		Expect(25);
		if (StartOf(3)) {
			VarProduct();
		} else if (la.kind == 3) {
			FuncCall();
		} else SynErr(51);
		Expect(17);
		Storage.Tree.Up(); 
	}

	void Write() {
		Storage.Tree.Down("Write", "RETURN", t); 
		Expect(26);
		if (StartOf(3)) {
			VarProduct();
		} else if (la.kind == 3) {
			FuncCall();
		} else SynErr(52);
		Expect(17);
		Storage.Tree.Up(); 
	}

	void Assume() {
		Storage.Tree.Down("Assume", "", t); 
		VarName();
		Expect(24);
		if (la.kind == 8) {
			AlCalc();
		}
		if (StartOf(3)) {
			VarProduct();
		} else if (la.kind == 3) {
			FuncCall();
		} else SynErr(53);
		while (StartOf(4)) {
			Calc();
			VarProduct();
		}
		Expect(17);
		Storage.Tree.Up(); 
	}

	void If() {
		Storage.Tree.Down("If", "IF", t); 
		Expect(31);
		Expect(11);
		BoolExp();
		Expect(14);
		Expect(15);
		Action();
		Expect(16);
		Expect(17);
		Storage.Tree.Up(); 
		if (la.kind == 34) {
			Else();
		}
	}

	void While() {
		Storage.Tree.Down("While", "TWIST", t); 
		Expect(35);
		Expect(11);
		BoolExp();
		Expect(14);
		Expect(15);
		Action();
		while (StartOf(2)) {
			Action();
		}
		Expect(16);
		Expect(17);
		Storage.Tree.Up(); 
	}

	void FuncCall() {
		Expect(3);
		Storage.Tree.Down("FuncCall", t.val, t); 
		Expect(12);
		while (StartOf(3)) {
			VarProduct();
			Expect(17);
		}
		Expect(13);
		Storage.Tree.Up(); 
	}

	void AlCalc() {
		Expect(8);
		Storage.Tree.Down("Calc", t.val, t); 
		Storage.Tree.Up(); 
	}

	void Calc() {
		if (la.kind == 7) {
			Get();
		} else if (la.kind == 8) {
			Get();
		} else if (la.kind == 9) {
			Get();
		} else if (la.kind == 10) {
			Get();
		} else SynErr(54);
		Storage.Tree.Down("Calc", t.val, t); 
		Storage.Tree.Up(); 
	}

	void BoolExp() {
		Storage.Tree.Down("BoolExp", "", t); 
		VarProduct();
		while (StartOf(5)) {
			BoolSign();
			VarProduct();
		}
		Storage.Tree.Up(); 
	}

	void Else() {
		Storage.Tree.Down("Else", "ELSE", t); 
		Expect(34);
		Expect(15);
		Action();
		while (StartOf(2)) {
			Action();
		}
		Expect(16);
		Expect(17);
		Storage.Tree.Up(); 
	}

	void BoolSign() {
		switch (la.kind) {
		case 21: {
			Get();
			break;
		}
		case 22: {
			Get();
			break;
		}
		case 23: {
			Get();
			break;
		}
		case 19: {
			Get();
			break;
		}
		case 18: {
			Get();
			break;
		}
		case 42: {
			Get();
			break;
		}
		default: SynErr(55); break;
		}
		Storage.Tree.Down("BoolSign", t.val, t); 
		Storage.Tree.Up(); 
	}



	public void Parse() {
		la = new Token();
		la.val = "";		
		Get();
	    Test();
		Expect(0);

	}
	
	static readonly bool[,] set = {
		{_T,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _T,_T,_T,_T, _x,_x,_x,_x, _x},
		{_x,_T,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_T,_T,_x, _x,_x,_x,_T, _x,_x,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_x,_T,_T,_x, _T,_T,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_T, _T,_T,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_T,_T, _x,_T,_T,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_T,_x, _x}

	};
} // end Parser


public class Errors {
	public int count = 0;                                    // number of errors detected
	public System.IO.TextWriter errorStream = Console.Out;   // error messages go to this stream
	public string errMsgFormat = "-- line {0} col {1}: {2}"; // 0=line, 1=column, 2=text

	public virtual void SynErr (int line, int col, int n) {
		string s;
		switch (n) {
			case 0: s = "EOF expected"; break;
			case 1: s = "ident expected"; break;
			case 2: s = "arrc expected"; break;
			case 3: s = "funcident expected"; break;
			case 4: s = "intcon expected"; break;
			case 5: s = "realcon expected"; break;
			case 6: s = "strcon expected"; break;
			case 7: s = "plus expected"; break;
			case 8: s = "minus expected"; break;
			case 9: s = "times expected"; break;
			case 10: s = "div expected"; break;
			case 11: s = "startbool expected"; break;
			case 12: s = "startfunc expected"; break;
			case 13: s = "endfunc expected"; break;
			case 14: s = "endbool expected"; break;
			case 15: s = "begin expected"; break;
			case 16: s = "end expected"; break;
			case 17: s = "semicol expected"; break;
			case 18: s = "startpoint expected"; break;
			case 19: s = "endpoint expected"; break;
			case 20: s = "blocksize expected"; break;
			case 21: s = "and expected"; break;
			case 22: s = "or expected"; break;
			case 23: s = "not expected"; break;
			case 24: s = "assumeSign expected"; break;
			case 25: s = "readName expected"; break;
			case 26: s = "writeName expected"; break;
			case 27: s = "varBlockName expected"; break;
			case 28: s = "funcBlockName expected"; break;
			case 29: s = "returnName expected"; break;
			case 30: s = "funcListName expected"; break;
			case 31: s = "ifName expected"; break;
			case 32: s = "mainNameStart expected"; break;
			case 33: s = "mainNameFin expected"; break;
			case 34: s = "elseName expected"; break;
			case 35: s = "whileName expected"; break;
			case 36: s = "array expected"; break;
			case 37: s = "int expected"; break;
			case 38: s = "float expected"; break;
			case 39: s = "string expected"; break;
			case 40: s = "\"BEGIN\" expected"; break;
			case 41: s = "\"TERMINATE\" expected"; break;
			case 42: s = "\"=\" expected"; break;
			case 43: s = "??? expected"; break;
			case 44: s = "invalid Action"; break;
			case 45: s = "invalid VarName"; break;
			case 46: s = "invalid VarProduct"; break;
			case 47: s = "invalid blockType"; break;
			case 48: s = "invalid Type"; break;
			case 49: s = "invalid VarDeclare"; break;
			case 50: s = "invalid FuncRetType"; break;
			case 51: s = "invalid Read"; break;
			case 52: s = "invalid Write"; break;
			case 53: s = "invalid Assume"; break;
			case 54: s = "invalid Calc"; break;
			case 55: s = "invalid BoolSign"; break;

			default: s = "error " + n; break;
		}
		errorStream.WriteLine(errMsgFormat, line, col, s);
		count++;
	}

	public virtual void SemErr (int line, int col, string s) {
		errorStream.WriteLine(errMsgFormat, line, col, s);
		count++;
	}
	
	public virtual void SemErr (string s) {
		errorStream.WriteLine(s);
		count++;
	}
	
	public virtual void Warning (int line, int col, string s) {
		errorStream.WriteLine(errMsgFormat, line, col, s);
	}
	
	public virtual void Warning(string s) {
		errorStream.WriteLine(s);
	}
} // Errors


public class FatalError: Exception {
	public FatalError(string m): base(m) {}
}
}