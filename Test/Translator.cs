﻿using System;
using System.Linq;

namespace Test
{
    public static class Translator
    {
        private static string _fieldInit = "";
        private static int _labelId = 0;
        private static bool _isIf = false;

        public static string GetCil()
        {
            string result = $"{XmlGateway.GetByKey("HEADER").Begin.Replace("replacewithnamespace", Storage.GetProjectNamespace)}{XmlGateway.GetByKey("START").Begin}{Storage.GetProjectNamespace}.Program {XmlGateway.GetByKey("START").End}\r\n";
            foreach (var t in Storage.IdentifiersDict.Values)
            {
                result += GetFieldName(t, $"G_{t.Name}");
                if (t.ArrayType != null)
                {
                    _fieldInit += _fieldInit == ""
                        ? $"{XmlGateway.GetByKey("MEMORY").Begin}\r\nldc.i4.s {t.Value.IValue}\r\nnewarr [mscorlib]System.{Storage.LsLangToDotNetTypes[t.ArrayType]}\r\nstsfld {Storage.LsLangToCilTypes[t.ArrayType]}[] {Storage.GetProjectNamespace}.Program::G_{t.Name}\r\n"
                        : $"ldc.i4.s {t.Value.IValue}\r\nnewarr [mscorlib]System.{Storage.LsLangToDotNetTypes[t.ArrayType]}\r\nstsfld {Storage.LsLangToCilTypes[t.ArrayType]}[] {Storage.GetProjectNamespace}.Program::G_{t.Name}\r\n";
                }
            }
            foreach (var t in Storage.ActionsDict.Values)
            {
                foreach (var innerT in t.Vars.Values)
                {
                    var uniqName = $"{t.Name}_{innerT.Name}";
                    result += GetFieldName(innerT, uniqName);
                }
            }
            foreach (var t in Storage.ActionsDict.Values)
            {
                result += GetActionContent(t);
            }
            result += GetActionContent(); //Main
            result += _fieldInit + "ret\r\n}\r\n}\r\n";
            return result.Replace("&", "").Replace("!", "");
        }

        private static string GetFieldName(Variable token, string name)
        {
            var result = ".field private static ";
            if (token.ArrayType == null)
            {
                result += $"{Storage.LsLangToCilTypes[token.Type]} {name}\r\n";
            }
            else if (token.Type == "ARRAY")
            {
                result += $"{Storage.LsLangToCilTypes[token.ArrayType]}[] {name}\r\n";
            }
            return result;
        }

        private static string GetActionContent(Action action = null) // Тело метода
        {
            Action trsAction = action ?? new Action("Main", "void");
            string result = $"{XmlGateway.GetByKey("ACTION").Begin} {trsAction.Name} {XmlGateway.GetByKey("ACTION").End}\r\n";
            var content = Storage.Tree.Root.GetByValue(trsAction.Name != "Main" ? trsAction.Name : "LAUNCH").FirstOrDefault();
            if (content?.Name == "FuncName") // Если функция, а не Main, то получаем содержимое
            {
                content = content.Parent.GetByName("FuncStatements").FirstOrDefault();
            }
            else
            {
                result += ".entrypoint\r\n"; // Помечаем как точку входа
            }
            if (content != null)
            {
                result += GetSmallBody(content, trsAction);
            }
            return result + "ret\r\n}\r\n";
        }

        private static string GetSmallBody(Node statement, Action trsAction)
        {
            string result = "";
            foreach (var pos in statement.Children)
            {
                if ((_isIf) && (pos.Name != "Else"))
                {
                    result += $"IL_{_labelId}:\r\n";
                    _labelId++;
                }
                _isIf = false;
                switch (pos.Name)
                {
                    case "Assume":
                        result += GetAssumeContent(pos, trsAction);
                        break;
                    case "If":
                        result += GetIfClause(pos, trsAction);
                        _isIf = true;
                        break;
                    case "Else":
                        result += GetSmallBody(pos, trsAction) + $"IL_{_labelId}:\r\n";
                        _labelId++;
                        break;
                    case "While":
                        result += GetWhileClause(pos, trsAction);
                        break;
                    case "Write":
                    {
                        result += StackOperations.Loader(pos.Children.First(), trsAction);
                        var posType =
                            GetUniqueName(trsAction, pos.Children.First().Value).Contains(pos.Children.First().Value.GetHashCode().ToString())
                                ? trsAction.Vars[pos.Children.First().Value].Type
                                : Storage.IdentifiersDict[pos.Children.First().Value].Type;
                        result += $"call string [mscorlib]System.Convert::ToString({Storage.LsLangToCilTypes[posType]})\r\n";
                        result += "call void[mscorlib] System.Console::WriteLine(string)\r\n";
                        break;
                    }
                    case "Read":
                    {
                        result += "call string [mscorlib]System.Console::ReadLine()\r\n";
                        var posType =
                            GetUniqueName(trsAction, pos.Children.First().Value).Contains(pos.Children.First().Value.GetHashCode().ToString())
                                ? trsAction.Vars[pos.Children.First().Value].Type
                                : Storage.IdentifiersDict[pos.Children.First().Value].Type;
                        result += $"call {Storage.LsLangToCilTypes[posType]} [mscorlib]System.Convert::To{Storage.LsLangToDotNetTypes[posType]}(string)\r\n";
                        result += StackOperations.Setter(pos.Children.First(), trsAction);
                        break;
                    }
                    default:
                        break;
                }
            }
            return result;
        }

        private static string GetIfClause(Node statement, Action trsAction)
        {
            var st = statement;
            string ifBody = "";
            string result = "";
            int counter = 0;
            string perfOp = String.Empty;
            foreach (var child in statement.Children.First().Children)
            {
                if (counter > 0)
                {
                    if (child.Name != "BoolSign")
                    {
                        result += StackOperations.Loader(child, trsAction);
                        if (perfOp != String.Empty)
                        {
                            result += $"{Storage.LsLangToCilOperations[perfOp]}\r\n";
                            perfOp = String.Empty;
                        }
                    }
                    else
                        perfOp = child.Value;
                }
                counter++;
            }
            result += $"brfalse IL_{_labelId}\r\n";
            st.Children.Remove(st.Children.First());
            ifBody += GetSmallBody(st, trsAction);
            result = $"{result}{ifBody}\r\nbr.s IL_{_labelId + 1}\r\nIL_{_labelId}:\r\n";
            _labelId++;
            return result;
        }

        private static string GetWhileClause(Node statement, Action trsAction)
        {
            var st = statement;
            string ifBody = "";
            string result = $"IL_{_labelId}:\r\n";
            int counter = 0;
            string perfOp = String.Empty;
            foreach (var child in statement.Children.First().Children)
            {
                if (counter > 0)
                {
                    if (child.Name != "BoolSign")
                    {
                        result += StackOperations.Loader(child, trsAction);
                        if (perfOp != String.Empty)
                        {
                            result += $"{Storage.LsLangToCilOperations[perfOp]}\r\n";
                            perfOp = String.Empty;
                        }
                    }
                    else
                        perfOp = child.Value;
                }
                counter++;
            }
            st.Children.Remove(st.Children.First());
            ifBody += $"{GetSmallBody(st, trsAction)}\r\nbrtrue IL_{_labelId}\r\n";
            result = $"{result}{ifBody}";
            _labelId++;
            return result;
        }

        private static string GetAssumeContent(Node statement, Action action = null)
        {
            string result = "";
            if (statement.Children.Count == 2)
            {
                result += StackOperations.Loader(statement.Children.First(), action);
                result += StackOperations.Loader(statement.Children.Last(), action);
                result += StackOperations.Setter(statement.Children.First(), action);
            }
            else
            {
                StackOperations.Loader(statement.Children.First(), action);
                int counter = 0;
                string perfOp = String.Empty;
                foreach (var child in statement.Children)
                {
                    if (counter > 0)
                    {
                        if (child.Name != "Calc")
                        {
                            result += StackOperations.Loader(child, action);
                            if (perfOp != String.Empty)
                            {
                                result += $"{Storage.LsLangToCilOperations[perfOp]}\r\n";
                                perfOp = String.Empty;
                            }
                        }
                        else
                            perfOp = child.Value;
                    }
                    counter++;
                }
                StackOperations.Setter(statement.Children.First(), action);
            }
            return result;
        }

        public static string GetUniqueName(Action action, string varName)
        {
            if (action!= null && (action.Vars.ContainsKey(varName)))
                return $"{action.Name}_{varName}";
            else if (Storage.IdentifiersDict.ContainsKey(varName))
                return $"G_{varName}";
            else
                return String.Empty;
        }
    }
}
