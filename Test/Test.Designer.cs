﻿namespace Test
{
    partial class Test
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ribbonStatusBar1 = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.VerifyBtn = new DevExpress.XtraBars.BarButtonItem();
            this.ShowTreeBtn = new DevExpress.XtraBars.BarButtonItem();
            this.statusLabel = new DevExpress.XtraBars.BarStaticItem();
            this.ShowTokens = new DevExpress.XtraBars.BarButtonItem();
            this.ShowIdent = new DevExpress.XtraBars.BarButtonItem();
            this.ShowCIL = new DevExpress.XtraBars.BarButtonItem();
            this.BuildApp = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.Sources = new Ionic.WinForms.RichTextBoxEx();
            this.Output = new System.Windows.Forms.RichTextBox();
            this.LoadBut = new System.Windows.Forms.Button();
            this.SaveBut = new System.Windows.Forms.Button();
            this.СheckBut = new System.Windows.Forms.Button();
            this.ShowTreeBut = new System.Windows.Forms.Button();
            this.ShowTokensBtn = new System.Windows.Forms.Button();
            this.ShowIdentBtn = new System.Windows.Forms.Button();
            this.ShowCILBtn = new System.Windows.Forms.Button();
            this.BuildBtn = new System.Windows.Forms.Button();
            this.LoadBtn = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonStatusBar1
            // 
            this.ribbonStatusBar1.Location = new System.Drawing.Point(0, 539);
            this.ribbonStatusBar1.Name = "ribbonStatusBar1";
            this.ribbonStatusBar1.Ribbon = this.ribbonControl1;
            this.ribbonStatusBar1.Size = new System.Drawing.Size(721, 21);
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ColorScheme = DevExpress.XtraBars.Ribbon.RibbonControlColorScheme.Blue;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.VerifyBtn,
            this.ShowTreeBtn,
            this.statusLabel,
            this.ShowTokens,
            this.ShowIdent,
            this.ShowCIL,
            this.BuildApp});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 8;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl1.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.True;
            this.ribbonControl1.ShowQatLocationSelector = false;
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(721, 146);
            this.ribbonControl1.StatusBar = this.ribbonStatusBar1;
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            // 
            // VerifyBtn
            // 
            this.VerifyBtn.Caption = "Проверить";
            this.VerifyBtn.Id = 3;
            this.VerifyBtn.Name = "VerifyBtn";
            this.VerifyBtn.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.VerifyBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.VerifyBtn_ItemClick);
            // 
            // ShowTreeBtn
            // 
            this.ShowTreeBtn.Caption = "Показать дерево";
            this.ShowTreeBtn.Id = 4;
            this.ShowTreeBtn.Name = "ShowTreeBtn";
            this.ShowTreeBtn.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.ShowTreeBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.VerifyBtn_ItemClick);
            // 
            // statusLabel
            // 
            this.statusLabel.Id = 1;
            this.statusLabel.Name = "statusLabel";
            // 
            // ShowTokens
            // 
            this.ShowTokens.Caption = "Показать токены";
            this.ShowTokens.Id = 3;
            this.ShowTokens.Name = "ShowTokens";
            this.ShowTokens.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.VerifyBtn_ItemClick);
            // 
            // ShowIdent
            // 
            this.ShowIdent.Caption = "Идентификаторы";
            this.ShowIdent.Id = 4;
            this.ShowIdent.Name = "ShowIdent";
            this.ShowIdent.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.VerifyBtn_ItemClick);
            // 
            // ShowCIL
            // 
            this.ShowCIL.Caption = "Вывести CIL";
            this.ShowCIL.Enabled = false;
            this.ShowCIL.Id = 6;
            this.ShowCIL.Name = "ShowCIL";
            this.ShowCIL.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ShowCIL_ItemClick);
            // 
            // BuildApp
            // 
            this.BuildApp.Caption = "Сборка";
            this.BuildApp.Enabled = false;
            this.BuildApp.Id = 7;
            this.BuildApp.Name = "BuildApp";
            this.BuildApp.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BuildApp_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "Главная";
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "Office 2016 Colorful";
            // 
            // Sources
            // 
            this.Sources.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Sources.Location = new System.Drawing.Point(12, 61);
            this.Sources.Name = "Sources";
            this.Sources.NumberAlignment = System.Drawing.StringAlignment.Center;
            this.Sources.NumberBackground1 = System.Drawing.SystemColors.ControlLight;
            this.Sources.NumberBackground2 = System.Drawing.SystemColors.Window;
            this.Sources.NumberBorder = System.Drawing.SystemColors.ControlDark;
            this.Sources.NumberBorderThickness = 1F;
            this.Sources.NumberColor = System.Drawing.Color.DarkGray;
            this.Sources.NumberFont = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sources.NumberLeadingZeroes = false;
            this.Sources.NumberLineCounting = Ionic.WinForms.RichTextBoxEx.LineCounting.CRLF;
            this.Sources.NumberPadding = 2;
            this.Sources.ShowLineNumbers = true;
            this.Sources.Size = new System.Drawing.Size(594, 335);
            this.Sources.TabIndex = 2;
            this.Sources.Text = "";
            // 
            // Output
            // 
            this.Output.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Output.Location = new System.Drawing.Point(12, 402);
            this.Output.Name = "Output";
            this.Output.ReadOnly = true;
            this.Output.Size = new System.Drawing.Size(594, 131);
            this.Output.TabIndex = 3;
            this.Output.Text = "";
            // 
            // LoadBut
            // 
            this.LoadBut.Location = new System.Drawing.Point(612, 59);
            this.LoadBut.Name = "LoadBut";
            this.LoadBut.Size = new System.Drawing.Size(97, 23);
            this.LoadBut.TabIndex = 6;
            this.LoadBut.Text = "Load";
            this.LoadBut.UseVisualStyleBackColor = true;
            this.LoadBut.Click += new System.EventHandler(this.LoadBut_Click);
            // 
            // SaveBut
            // 
            this.SaveBut.Location = new System.Drawing.Point(612, 89);
            this.SaveBut.Name = "SaveBut";
            this.SaveBut.Size = new System.Drawing.Size(97, 23);
            this.SaveBut.TabIndex = 7;
            this.SaveBut.Text = "Save";
            this.SaveBut.UseVisualStyleBackColor = true;
            this.SaveBut.Click += new System.EventHandler(this.SaveBut_Click);
            // 
            // СheckBut
            // 
            this.СheckBut.Location = new System.Drawing.Point(612, 133);
            this.СheckBut.Name = "СheckBut";
            this.СheckBut.Size = new System.Drawing.Size(97, 23);
            this.СheckBut.TabIndex = 8;
            this.СheckBut.Text = "Сheck";
            this.СheckBut.UseVisualStyleBackColor = true;
            this.СheckBut.Click += new System.EventHandler(this.СheckBut_Click);
            // 
            // ShowTreeBut
            // 
            this.ShowTreeBut.Location = new System.Drawing.Point(612, 175);
            this.ShowTreeBut.Name = "ShowTreeBut";
            this.ShowTreeBut.Size = new System.Drawing.Size(97, 23);
            this.ShowTreeBut.TabIndex = 9;
            this.ShowTreeBut.Text = "Show Tree";
            this.ShowTreeBut.UseVisualStyleBackColor = true;
            this.ShowTreeBut.Click += new System.EventHandler(this.ShowTreeBut_Click);
            // 
            // ShowTokensBtn
            // 
            this.ShowTokensBtn.Location = new System.Drawing.Point(612, 205);
            this.ShowTokensBtn.Name = "ShowTokensBtn";
            this.ShowTokensBtn.Size = new System.Drawing.Size(97, 23);
            this.ShowTokensBtn.TabIndex = 10;
            this.ShowTokensBtn.Text = "Show Tokens";
            this.ShowTokensBtn.UseVisualStyleBackColor = true;
            this.ShowTokensBtn.Click += new System.EventHandler(this.ShowTokensBtn_Click);
            // 
            // ShowIdentBtn
            // 
            this.ShowIdentBtn.Location = new System.Drawing.Point(612, 235);
            this.ShowIdentBtn.Name = "ShowIdentBtn";
            this.ShowIdentBtn.Size = new System.Drawing.Size(97, 23);
            this.ShowIdentBtn.TabIndex = 11;
            this.ShowIdentBtn.Text = "Show Ident";
            this.ShowIdentBtn.UseVisualStyleBackColor = true;
            this.ShowIdentBtn.Click += new System.EventHandler(this.ShowIdentBtn_Click);
            // 
            // ShowCILBtn
            // 
            this.ShowCILBtn.Location = new System.Drawing.Point(612, 265);
            this.ShowCILBtn.Name = "ShowCILBtn";
            this.ShowCILBtn.Size = new System.Drawing.Size(97, 23);
            this.ShowCILBtn.TabIndex = 12;
            this.ShowCILBtn.Text = "Show CIL";
            this.ShowCILBtn.UseVisualStyleBackColor = true;
            this.ShowCILBtn.Click += new System.EventHandler(this.ShowCILBtn_Click);
            // 
            // BuildBtn
            // 
            this.BuildBtn.Location = new System.Drawing.Point(612, 310);
            this.BuildBtn.Name = "BuildBtn";
            this.BuildBtn.Size = new System.Drawing.Size(97, 23);
            this.BuildBtn.TabIndex = 13;
            this.BuildBtn.Text = "Build";
            this.BuildBtn.UseVisualStyleBackColor = true;
            this.BuildBtn.Click += new System.EventHandler(this.BuildBtn_Click);
            // 
            // LoadBtn
            // 
            this.LoadBtn.Caption = "Загрузить";
            this.LoadBtn.Id = 1;
            this.LoadBtn.Name = "LoadBtn";
            this.LoadBtn.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.LoadBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.LoadBtn_ItemClick);
            // 
            // Test
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(721, 560);
            this.Controls.Add(this.BuildBtn);
            this.Controls.Add(this.ShowCILBtn);
            this.Controls.Add(this.ShowIdentBtn);
            this.Controls.Add(this.ShowTokensBtn);
            this.Controls.Add(this.ShowTreeBut);
            this.Controls.Add(this.СheckBut);
            this.Controls.Add(this.SaveBut);
            this.Controls.Add(this.LoadBut);
            this.Controls.Add(this.Output);
            this.Controls.Add(this.Sources);
            this.Controls.Add(this.ribbonStatusBar1);
            this.Controls.Add(this.ribbonControl1);
            this.MinimumSize = new System.Drawing.Size(695, 561);
            this.Name = "Test";
            this.Ribbon = this.ribbonControl1;
            this.StatusBar = this.ribbonStatusBar1;
            this.Text = "MainForm";
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.LangLs_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.LangLs_DragEnter);
            this.Resize += new System.EventHandler(this.LangLS_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar1;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private System.Windows.Forms.RichTextBox Output;
        private Ionic.WinForms.RichTextBoxEx Sources;
        private System.Windows.Forms.Button LoadBut;
        private System.Windows.Forms.Button SaveBut;
        private System.Windows.Forms.Button СheckBut;
        private System.Windows.Forms.Button ShowTreeBut;
        private System.Windows.Forms.Button ShowTokensBtn;
        private System.Windows.Forms.Button ShowIdentBtn;
        private System.Windows.Forms.Button ShowCILBtn;
        private System.Windows.Forms.Button BuildBtn;
        private DevExpress.XtraBars.BarButtonItem LoadBtn;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.BarButtonItem VerifyBtn;
        private DevExpress.XtraBars.BarButtonItem ShowTreeBtn;
        private DevExpress.XtraBars.BarStaticItem statusLabel;
        private DevExpress.XtraBars.BarButtonItem ShowTokens;
        private DevExpress.XtraBars.BarButtonItem ShowIdent;
        private DevExpress.XtraBars.BarButtonItem ShowCIL;
        private DevExpress.XtraBars.BarButtonItem BuildApp;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
    }
}

