﻿using System.Collections.Generic;
using System.Linq;

namespace Test
{
    public class Action
    {
        public string Name;
        public string ReturnType;
        public Dictionary<string, Variable> Vars;
        public Action()
        {

        }

        public Action(string name, string retType)
        {
            Name = name;
            ReturnType = retType;
            this.Vars = new Dictionary<string, Variable>();
        }

        public List<string> GetInputTypes()
        {
            return Vars.Select(var => var.Value.Type).ToList();
        }
    }
}
