﻿using System;

namespace Test
{
    public class Variable
    {
        public string Name;
        public string Type;
        public string ArrayType;
        public VarValue Value;
        public Variable()
        {

        }

        public Variable(string name, string type, string arrayType = null)
        {
            Value.IValue = Int32.MaxValue;
            Value.FValue = Double.MaxValue;
            Name = name;
            Type = type;
            ArrayType = arrayType;
        }
    }
    public struct VarValue
    {
        public int IValue;
        public double FValue;
        public string SValue;
    }
}
