﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using DevExpress.XtraBars;

namespace Test
{
    public partial class Test : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        private int _height = 561;
        public Test()
        {
            InitializeComponent();
            ribbonControl1.MinimizedChanged += ExpandCollapseItem_ItemClick;
            if (Program.AppArguments.Length > 0)
            {
                if (File.Exists(Program.AppArguments[0]))
                {
                    LoadFile(Program.AppArguments[0]);
                }
            }
        }

        private void LangLs_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = e.Data.GetDataPresent(DataFormats.FileDrop) ? DragDropEffects.Copy : DragDropEffects.None;
        }

        private void LangLs_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var files = (string[])e.Data.GetData(DataFormats.FileDrop);
                if (files.Length > 0)
                {
                    if (File.Exists(files[0]))
                    {
                        LoadFile(files[0]);
                    }
                }
            }
        }

        private void VerifyBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (var conWriter = new StringWriter())
            {
                Console.SetOut(conWriter);
                using (var ms = new MemoryStream())
                {
                    using (var sw = new StreamWriter(ms))
                    {
                        sw.Write(Sources.Text);
                        sw.Flush();

                        // Неявное использование потока
                        ms.Position = 0;
                        var scanner = new Scanner(ms);
                        var parser = new Parser(scanner);
                        Semantic semanticAnalyzer = new Semantic();
                        parser.Parse();
                        var tree = Storage.Tree.SetIndentGlobal(0);
                        conWriter.Flush();
                        //******************************

                        switch (e.Item.Name)
                        {
                            case "VerifyBtn":
                                Output.Text = conWriter.GetStringBuilder().ToString();
                                if (parser.errors.count == 0)
                                {
                                    Storage.IdentifiersDict = new Dictionary<string, Variable>();
                                    Storage.ActionsDict = new Dictionary<string, Action>();
                                    Output.Text += semanticAnalyzer.Analize(Storage.Tree.Root);
                                    if (Output.Text.Length == 0)
                                    {
                                        ShowCIL.Enabled = true;
                                        BuildApp.Enabled = true;
                                    }
                                }
                                break;
                            case "ShowTreeBtn":
                                Output.Text = tree;
                                break;
                            case "ShowTokens":
                                Output.Text = Storage.Tokens.Stringify();
                                break;
                            case "ShowIdent":
                                if (parser.errors.count == 0)
                                {
                                    semanticAnalyzer.Analize(Storage.Tree.Root); // Init
                                    Output.Text = Storage.IdentifiersDict.Stringify();
                                }
                                else
                                {
                                    Output.Text = 
                                        "Невозможно вывести таблицу идентификаторов. \r\n" + 
                                            "В процессе работы парсера были обнаружены следующие ошибки: \r\n" + 
                                                $" {conWriter.GetStringBuilder()}\r\n. " + 
                                                    "Продолжите после исправления.";
                                }
                                break;
                            default:
                                throw new InvalidOperationException("Метод соответствующий запросу не обнаружен");
                        }
                        statusLabel.Caption = $"Обнаружено ошибок парсера: {parser.errors.count}";
                    }
                }
            }
        }

        private void ShowCIL_ItemClick(object sender, ItemClickEventArgs e)
        {
            Output.Text = Translator.GetCil();
        }

        private void BuildApp_ItemClick(object sender, ItemClickEventArgs e)
        {
            string strPath = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            SaveFile($"{Path.Combine(strPath, Storage.GetProjectNamespace)}.il", Translator.GetCil());
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.WorkingDirectory = XmlGateway.GetByKey("BUILDER").Begin;
            startInfo.FileName = "ilasm.exe";
            startInfo.Arguments = $"/exe {Path.Combine(strPath, Storage.GetProjectNamespace)}.il /output={Path.Combine(strPath, Storage.GetProjectNamespace)}.exe";
            Process.Start(startInfo);
        }

        private void LoadBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (var openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "Test files (*.lls)|*.lls|Text files (*.txt)|*.txt|All files (*.*)|*.*";
               // openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                openFileDialog.DefaultExt = "lls";
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                   LoadFile(openFileDialog.FileName);
                }
            }
        }

        private void LoadFile(string path)
        {
            using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                using (var sr = new StreamReader(fs))
                {
                    Sources.Text = sr.ReadToEnd();
                }
            }
            ShowCIL.Enabled = false;
            BuildApp.Enabled = false;
        }

        private void SaveBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (var saveFileDialog = new SaveFileDialog())
            {
                saveFileDialog.Filter = "Test files (*.lls)|*.lls|Text files (*.txt)|*.txt|All files (*.*)|*.*";
                saveFileDialog.AddExtension = true;
                saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                saveFileDialog.DefaultExt = "lls";
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    SaveFile(saveFileDialog.FileName, Sources.Text);
                }
            }
        }

        private void SaveFile(string path, string content)
        {
            using (var fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                using (var sw = new StreamWriter(fs))
                {
                    sw.Write(content);
                }
            }
        }

        private void ExpandCollapseItem_ItemClick(object sender, EventArgs e)
        {
            Sources.Top = ribbonControl1.Minimized ? 64 : 156;
            Sources.Height += ribbonControl1.Minimized ? 64 : -64;
        }

        private void LangLS_Resize(object sender, EventArgs e)
        {
            if (Height > 300)
            {
                var delta = (Height - _height);
                Output.Height += delta / 2;
                Output.Top -= delta / 2;
                Sources.Height += delta / 2;
                _height = Height;
            }
        }

        private void BuildBtn_Click(object sender, EventArgs e)
        {
            string strPath = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            SaveFile($"{Path.Combine(strPath, Storage.GetProjectNamespace)}.il", Translator.GetCil());
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.WorkingDirectory = XmlGateway.GetByKey("BUILDER").Begin;
            startInfo.FileName = "ilasm.exe";
            startInfo.Arguments = $"/exe {Path.Combine(strPath, Storage.GetProjectNamespace)}.il /output={Path.Combine(strPath, Storage.GetProjectNamespace)}.exe";
            Process.Start(startInfo);
        }

        private void ShowCILBtn_Click(object sender, EventArgs e)
        {
            Output.Text = Translator.GetCil();
        }

        private void ShowIdentBtn_Click(object sender, EventArgs e)
        {
            using (var conWriter = new StringWriter())
            {
                Console.SetOut(conWriter);
                using (var ms = new MemoryStream())
                {
                    using (var sw = new StreamWriter(ms))
                    {
                        sw.Write(Sources.Text);
                        sw.Flush();

                        // Неявное использование потока
                        ms.Position = 0;
                        var scanner = new Scanner(ms);
                        var parser = new Parser(scanner);
                        Semantic semanticAnalyzer = new Semantic();
                        parser.Parse();
                        var tree = Storage.Tree.SetIndentGlobal(0);
                        conWriter.Flush();
                        //******************************
                        if (parser.errors.count == 0)
                        {
                            semanticAnalyzer.Analize(Storage.Tree.Root); // Init
                            Output.Text = Storage.IdentifiersDict.Stringify();
                        }
                        else
                        {
                            Output.Text =
                                "Невозможно вывести таблицу идентификаторов. \r\n" +
                                    "В процессе работы парсера были обнаружены следующие ошибки: \r\n" +
                                        $" {conWriter.GetStringBuilder()}\r\n. " +
                                            "Продолжите после исправления.";
                        }
                        statusLabel.Caption = $"Обнаружено ошибок парсера: {parser.errors.count}";
                    }
                }
            }
        }

        private void ShowTokensBtn_Click(object sender, EventArgs e)
        {
            using (var conWriter = new StringWriter())
            {
                Console.SetOut(conWriter);
                using (var ms = new MemoryStream())
                {
                    using (var sw = new StreamWriter(ms))
                    {
                        sw.Write(Sources.Text);
                        sw.Flush();
                        // Неявное использование потока
                        ms.Position = 0;
                        var scanner = new Scanner(ms);
                        var parser = new Parser(scanner);
                        Semantic semanticAnalyzer = new Semantic();
                        parser.Parse();
                        var tree = Storage.Tree.SetIndentGlobal(0);
                        conWriter.Flush();
                        Output.Text = Storage.Tokens.Stringify();
                        statusLabel.Caption = $"Обнаружено ошибок парсера: {parser.errors.count}";
                    }
                }
            }
        }

        private void ShowTreeBut_Click(object sender, EventArgs e)
        {
            using (var conWriter = new StringWriter())
            {
                Console.SetOut(conWriter);
                using (var ms = new MemoryStream())
                {
                    using (var sw = new StreamWriter(ms))
                    {
                        sw.Write(Sources.Text);
                        sw.Flush();

                        // Неявное использование потока
                        ms.Position = 0;
                        var scanner = new Scanner(ms);
                        var parser = new Parser(scanner);
                        Semantic semanticAnalyzer = new Semantic();
                        parser.Parse();
                        var tree = Storage.Tree.SetIndentGlobal(0);
                        conWriter.Flush();
                        //******************************
                        Output.Text = tree;
                        statusLabel.Caption = $"Обнаружено ошибок парсера: {parser.errors.count}";
                    }
                }
            }
        }

        private void СheckBut_Click(object sender, EventArgs e)
        {
            using (var conWriter = new StringWriter())
            {
                Console.SetOut(conWriter);
                using (var ms = new MemoryStream())
                {
                    using (var sw = new StreamWriter(ms))
                    {
                        sw.Write(Sources.Text);
                        sw.Flush();

                        // Неявное использование потока
                        ms.Position = 0;
                        var scanner = new Scanner(ms);
                        var parser = new Parser(scanner);
                        Semantic semanticAnalyzer = new Semantic();
                        parser.Parse();
                        var tree = Storage.Tree.SetIndentGlobal(0);
                        conWriter.Flush();
                        //******************************

                        Output.Text = conWriter.GetStringBuilder().ToString();
                        if (parser.errors.count == 0)
                        {
                            Storage.IdentifiersDict = new Dictionary<string, Variable>();
                            Storage.ActionsDict = new Dictionary<string, Action>();
                            Output.Text += semanticAnalyzer.Analize(Storage.Tree.Root);
                            if (Output.Text.Length == 0)
                            {
                                ShowCIL.Enabled = true;
                                BuildApp.Enabled = true;
                            }
                        }
                        statusLabel.Caption = $"Обнаружено ошибок парсера: {parser.errors.count}";
                    }
                }
            }
        }

        private void SaveBut_Click(object sender, EventArgs e)
        {
            using (var saveFileDialog = new SaveFileDialog())
            {
                saveFileDialog.Filter = "Test files (*.lt)|*.lt|Text files (*.txt)|*.txt|All files (*.*)|*.*";
                saveFileDialog.AddExtension = true;
                saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                saveFileDialog.DefaultExt = "lt";
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    SaveFile(saveFileDialog.FileName, Sources.Text);
                }
            }
        }

        private void LoadBut_Click(object sender, EventArgs e)
        {
            using (var openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "Test files (*.lt)|*.lt|Text files (*.txt)|*.txt|All files (*.*)|*.*";
                // openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                openFileDialog.DefaultExt = "lt";
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    LoadFile(openFileDialog.FileName);
                }
            }
        }
    }
}
