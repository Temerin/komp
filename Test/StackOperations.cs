using System;
using System.Linq;

namespace Test
{
    public static class StackOperations
    {
        public static string Loader(Node statement, Action action = null)
        {
            if (statement.Name == "FuncCall")
                return CallFunction(statement, action);
            if (statement.Children.Any())
                return LoadArray(statement, action);
            if (Translator.GetUniqueName(action, statement.Value) == "") // �������� ��������� � ����
                return LoadConst(statement);
            return LoadField(statement, action); // �������� � ���� ����������
        }

        private static string CallFunction(Node statement, Action action)
        {
            string result = "";
            var currFunc = Storage.ActionsDict.FirstOrDefault(x => x.Key == statement.Value).Value;
            var list = currFunc.Vars.Values.ToList();
            for (int i = 0; i < statement.Children.Count; i++)
            {
                result += Loader(statement.Children[i], action);
                result += $"stsfld {Storage.LsLangToCilTypes[list[i].Type]} {Storage.GetProjectNamespace}.Program::{Translator.GetUniqueName(currFunc, list[i].Name)}\r\n";
            }
            result += $"call void {Storage.GetProjectNamespace}.Program::{statement.Value}()\r\n";
            return result;
        }

        private static string LoadConst(Node statement)
        {
            return $"{Storage.LsLangToCilDownload[statement.Name]}{statement.Value}\r\n";
        }

        private static string LoadField(Node statement, Action action)
        {
            return $"ldsfld {Storage.LsLangToCilTypes[Translator.GetUniqueName(action, statement.Value).Contains(action.Name) ? action.Vars[statement.Value].Type : Storage.IdentifiersDict[statement.Value].Type]} {Storage.GetProjectNamespace}.Program::{Translator.GetUniqueName(action, statement.Value)}\r\n";
        }

        private static string LoadArray(Node statement, Action action)
        {
            return $"ldsfld {Storage.LsLangToCilTypes[Translator.GetUniqueName(action, statement.Value).Contains(action.Name) ? action.Vars[statement.Value].ArrayType : Storage.IdentifiersDict[statement.Value].ArrayType]}[] {Storage.GetProjectNamespace}.Program::{Translator.GetUniqueName(action, statement.Value)}\r\n{Loader(statement.Children.First())}";
        }

        public static string Setter(Node statement, Action action = null)
        {
            if (!statement.Children.Any()) // �� ������� �������
                return SetField(statement, action);
            else
                return SetArray(statement);
        }

        private static string SetField(Node statement, Action action)
        {
            return $"stsfld {Storage.LsLangToCilTypes[Translator.GetUniqueName(action, statement.Value).Contains(action.Name) ? action.Vars[statement.Value].Type : Storage.IdentifiersDict[statement.Value].Type]} {Storage.GetProjectNamespace}.Program::{Translator.GetUniqueName(action, statement.Value)}\r\n";
        }

        private static string SetArray(Node statement)
        {
            switch (Storage.IdentifiersDict[statement.Value].ArrayType)
            {
                case "INT":
                    return "stelem.i4\r\n";
                case "VOYAGE":
                    return "stelem.r8\r\n";
                default:
                    return "\r\n";
            }
        }
    }
}